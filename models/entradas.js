const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment'); // 2. require mongoose-auto-increment
require("dotenv").config();



const entradaSchema = new Schema({
    costo: {
        type: Number,
        default: 0
    },
    clave: {
        type: String,
        default: ""
    },
    estatus: {
        type: String,
        default: "Disponible"
    },
    laboratorio:{
        type: Object
        
    },
    material:{
        type: Object
        
    },
});

autoIncrement.initialize(mongoose.connection); // 3. initialize autoIncrement 

entradaSchema.plugin(autoIncrement.plugin, 'entrada'); // 4. use autoIncrement
mongoose.model('entrada', entradaSchema);

module.exports = mongoose.model('entrada', entradaSchema);