const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment'); // 2. require mongoose-auto-increment
require("dotenv").config();



const prestamoSchema = new Schema({
    inicio: {
        type: String,
        default:""
    },
    fin:{
        type: String,
        default:""
    },
    estatus:{
        type: String,
        default:"Activo"
    },
    usuario:{
        type: Object
        
    },
    entradas_prestamo:{
        type: Array
    }
});

autoIncrement.initialize(mongoose.connection); // 3. initialize autoIncrement 

prestamoSchema.plugin(autoIncrement.plugin, 'prestamo'); // 4. use autoIncrement
mongoose.model('prestamo', prestamoSchema);

module.exports = mongoose.model('prestamo', prestamoSchema);