const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment'); // 2. require mongoose-auto-increment
require("dotenv").config();

const materialSchema = new Schema({
    // id:{
    //     type: Number,
    //     // required: true,
    //     default:""
    // },
    nombre:{
        type: String,
        default:""
    },
    descripcion:{
        type: String,
        default:""
    },
    estatus:{
        type: String,
        default:"Activo"
    },
    foto:{
        type: String,
        required: true,
    },    
    categoria:{
        type: Object
    }
});

autoIncrement.initialize(mongoose.connection); // 3. initialize autoIncrement 

materialSchema.plugin(autoIncrement.plugin, 'material'); // 4. use autoIncrement
mongoose.model('material', materialSchema);

module.exports = mongoose.model('material', materialSchema);