const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment'); // 2. require mongoose-auto-increment
require("dotenv").config();

const usuarioSchema = new Schema({
    // id:{
    //     type: Number,
    //     required: true,
    // },
    nombres:{
        type: String,
        // required: true,
        default:""
    },
    apellido1:{
        type: String,
        // required: true,
        default:""
    },
    apellido2:{
        type: String,
        // required: true,
        default:""
    },
    matricula:{
        type: String,
        // required: true,
        default:""
    },
    password:{
        type: String,
        // required: true,
        default:""
    },
    ciudad:{
        type: String,
        // required: true,
        default:""
    },
    colonia:{
        type: String,
        // required: true,
        default:""
    },
    cp:{
        type: Number,
        // required: true,
        default:""
    },
    calle:{
        type: String,
        // required: true,
        default:""
    },
    numeroInt:{
        type: String,
        // required: true,
        default:""
    },
    numeroExt:{
        type: String,
        // required: true,
        default:""
    },
    estatus:{
        type: String,
        // required: true,
        default:""
    },
    foto:{
        type: String,
        // required: true,
        default:""
    },
    rango:{
        type: Object
        // ref: "rango"
    },
    token:{
        type: String
    }  
});

autoIncrement.initialize(mongoose.connection); // 3. initialize autoIncrement 

usuarioSchema.plugin(autoIncrement.plugin, 'usuario'); // 4. use autoIncrement
mongoose.model('usuario', usuarioSchema);

module.exports = mongoose.model('usuario', usuarioSchema);