const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment'); // 2. require mongoose-auto-increment
require("dotenv").config();


// id: {
//   type: Number,
//   unique: true,
//   required: true
// },
const categoriaSchema = new Schema({
  nombre: {
    type: String,
    required: true
  },
  estatus: {
    type: String,
    required: true,
    default: "Activo"
  }
});
// , { collection: 'categorias' }

autoIncrement.initialize(mongoose.connection); // 3. initialize autoIncrement 

categoriaSchema.plugin(autoIncrement.plugin, 'categoria'); // 4. use autoIncrement
mongoose.model('categoria', categoriaSchema);

module.exports = mongoose.model('categoria', categoriaSchema);
