const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment'); // 2. require mongoose-auto-increment
require("dotenv").config();

const laboratorioSchema = new Schema({
    // id:{
    //     type: Number,
    //     // required: true,
    //     default:""
    // },
    nombre:{
        type: String,
        // required: true,
        default:""
    },
    localizacion:{
        type: String,
        // required: true,
        default:""
    },
    estatus:{
        type: String,
        default:"Activo"
    },
    usuario:{
        type: Object
        // ref: "usuario"
    }
});

autoIncrement.initialize(mongoose.connection); // 3. initialize autoIncrement 

laboratorioSchema.plugin(autoIncrement.plugin, 'laboratorio'); // 4. use autoIncrement
mongoose.model('laboratorio', laboratorioSchema);

module.exports = mongoose.model('laboratorio', laboratorioSchema);