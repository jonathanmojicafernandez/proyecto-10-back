const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment'); // 2. require mongoose-auto-increment
require("dotenv").config();


const rangoSchema = new Schema({
  id: {
    type: Number,
    required: true
  },
  nombre: {
    type: String,
    required: true
  }
});


autoIncrement.initialize(mongoose.connection); // 3. initialize autoIncrement 

rangoSchema.plugin(autoIncrement.plugin, 'rango'); // 4. use autoIncrement
mongoose.model('rango', rangoSchema);

module.exports = mongoose.model('rango', rangoSchema);
