const cron = require('node-cron');
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cors = require('cors');
var categoriaRouter = require('./routes/categorias');
var laboratorioRouter = require('./routes/laboratorios');
var usuarioRouter = require('./routes/usuarios');
var prestamoRouter = require('./routes/prestamos');
var entradaRouter = require('./routes/entradas');
var prestamoC = require('./controllers/prestamoController');
const materialRouter = require('./routes/materiales');
const loginController = require('./controllers/loginController');
const auth = require("./middleware/auth");
const rangoslRouter = require('./routes/rangos');
const consultasMaterialRouter = require('./routes/consultasMateriales');
var userRouter = require('./routes/user');
const consultasPrestamosRouter = require('./routes/consultasPrestamo');
require("dotenv").config();
var app = express();

app.use(cors());

mongoose.connect(process.env.DATABASE_URL);

app.use(express.json({limit:'50mb'}));
app.use(bodyParser.urlencoded({
  limit:'50mb',
  extended: true
}));
app.use(bodyParser.json());


app.post('/login',loginController.login);
app.use('/api',auth,categoriaRouter);
app.use('/api',auth,laboratorioRouter);
app.use('/api',auth,usuarioRouter);
app.use('/api',auth,materialRouter);
app.use('/api',auth,prestamoRouter);
app.use('/api',auth, entradaRouter);
app.use('/api',auth,rangoslRouter);
app.use('/movil',auth, consultasMaterialRouter);
app.use('/movile',userRouter);
app.use('/movil',auth, consultasPrestamosRouter);

app.listen(process.env.PORT, function () {
  console.log('Se encuentra ejecutnado en la siguiente ruta: '+process.env.IP +":"+ process.env.PORT);
});

cron.schedule('0 7 * * mon-fri', function() {
  prestamoC.changeDebt();
});
