const Usuario = require('../models/usuarios');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Rango = require('../models/rangos');

module.exports = {

    add: async function (request, response) {

        const { nombres, apellido1, apellido2, matricula, password, estatus} = request.body;
        //console.log(request.body.foto);

        if (!(matricula && password)) {
            return response.status(400).json({ response: "Es necesario llenar todos los campos" });
        }
        const userFind = await Usuario.findOne({ matricula: matricula });
        if (userFind) {
            return response.status(400).json({ response: "El usuario con esta matricula ya existe" });
        }
        const rango = await Rango.findOne({ id: 1 });
        if (rango) {
            encryptedPassword = await bcrypt.hash(password, 10);
            var user = await Usuario.create({
                nombres,
                apellido1,
                apellido2,
                matricula: matricula,
                estatus,
                password: encryptedPassword,
                rango
            });
            const token = jwt.sign(
                { user_id: user._id, matricula },
                process.env.TOKEN_KEY,
                {
                    expiresIn: "2h",
                }
            );
            user.token = token;
            user.save();
            return response.status(200).json(user);
        }
    },
    update: async function (request, response) {
        let usuarioId = request.body.idUsuario;

        var foto = '';

        try {
            const uploadedResponse = await cloudinary.uploader.upload(request.body.foto, {
                upload_preset: 'ml_default'
            })
            //console.log(uploadedResponse);
            foto = uploadedResponse.url;
        } catch (error) {
            console.error(error);
            response.status(500).json({ err: 'no se pudo cargar la imagen' });
        }


        const usuario = await Usuario.findOne({ _id: usuarioId });
        if (usuario) {
            usuario.nombres = request.body.nombres;
            usuario.apellido1 = request.body.apellido1;
            usuario.apellido2 = request.body.apellido2;
            usuario.matricula = request.body.matricula;
            if (request.body.password != null || request.body.password != undefined || request.body.password != '') {
                encryptedPassword = await bcrypt.hash(request.body.password, 10);
                usuario.password = encryptedPassword;
            }
            // usuario.password = request.body.password;
            usuario.ciudad = request.body.ciudad;
            usuario.colonia = request.body.colonia;
            usuario.cp = request.body.cp;
            usuario.calle = request.body.calle;
            usuario.numeroInt = request.body.numeroInt;
            usuario.numeroExt = request.body.numeroExt
            // usuario.estatus = request.body.estatus;
            usuario.foto = foto;
            //usuario.rango = newRango;

            usuario.save();
            return response.status(200).json({
                message: "Se ha modificado correctamente",
                user: user
            });
        }
        return response.status(404).json({
            message: 'No se encontro este registro.'
        });



    },
    login: async function (request, response) {
        try {
            const { matricula, password } = request.body;
            if (!(matricula && password)) {
                const objeto = {
                    "code": 400,
                    "response": "Debes llenar todos los datos"
                }
                return response.status(400).send(objeto);
            }
            const user = await Usuario.findOne({ matricula });

            if (user && (await bcrypt.compare(password, user.password))) {
                // Create token
                if (user.rango.id == 1) {
                    const token = jwt.sign(
                        { user_id: user._id, matricula },
                        process.env.TOKEN_KEY,
                        {
                            expiresIn: "2h",
                        }
                    );

                    // save user token
                    user.token = token;
                    user.save();
                    const objeto = {
                        // "code": 200,
                        "token": user.token,
                        "nombre": user.nombres
                    }
                    return response.status(200).json({
                        message: "Inicio de sesión correcto",
                        user: user
                    });
                } else {
                    const objeto = {
                        "code": 400,
                        "response": "No puedes acceder a esta seccion"
                    }
                    return response.status(400).send(objeto);
                }
            } else {
                const objeto = {
                    "code": 400,
                    "response": "Usuario o contraseña incorrectos"
                }
                return response.status(400).json(objeto);
            }

        } catch (e) {
            const objeto = {
                "status": 500,
                "response": "Ha ocurrodo un error, vuelve más tarde"
            }
            return response.status(500).json(objeto);
        }
    }
}