const Categoria = require('../models/categorias');
const Material = require('../models/materiales');
const { cloudinary } = require("../utils/cloudinary");

module.exports = {
    getAll: function(request, response) {
        Material.find(function(error, material) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            response.json(material);
        })
    },
    add: async function(request, response) {
        //console.log(request.body);
        var foto = '';

        if (request.body.foto == '') {
            foto = './defectoMaterial.png';

        } else {
            try {
                const uploadedResponse = await cloudinary.uploader.upload(request.body.foto, {
                        upload_preset: 'ml_default'
                    })
                    //console.log(uploadedResponse);
                foto = uploadedResponse.url;
            } catch (error) {
                console.error(error);
                response.status(500).json({ err: 'no se pudo cargar la imagen' });
            }
        }

        var categoriaId = Number(request.body.categoria);

        Categoria.findOne({ _id: categoriaId }, function(error, categoria) {
            console.log('categoria ' + categoria);
            if (error) {
                response.status(500).send(error);
                return;
            }
            if (categoria) {
                data = {
                    "nombre": request.body.nombre,
                    "descripcion": request.body.descripcion,
                    "estatus": request.body.estatus,
                    "foto": foto,
                    "categoria": categoria
                }

                var material = new Material(data);
                //console.log(material);
                material.save();
                response.json(data);

                return;
            }
            response.status(404).json({
                message: 'No se encontro este registro.'
            });
        });
    },
    update: async function(request, response) {
        let categoriaA = Categoria;
        let categoriaId = request.body.idA;
        let materialId = request.body.id;

        var foto = '';
        console.log(request.body.foto);
        if (request.body.foto == './defectoMaterial.png') {
            //foto = request.body.foto;
        } else {

            try {
                const uploadedResponse = await cloudinary.uploader.upload(request.body.foto, {
                        upload_preset: 'ml_default'
                    })
                    //console.log(uploadedResponse);
                foto = uploadedResponse.url;
            } catch (error) {
                console.error(error);
                response.status(500).json({ err: 'no se pudo cargar la imagen' });
            }
        }


        Categoria.findOne({ _id: categoriaId }, function(error, categoria) {
            // console.log('categoria '+categoria);
            if (error) {
                response.status(500).send(error);
            }
            if (categoria) {
                categoriaA = new Categoria(categoria);
                Material.findOne({ _id: materialId }, function(error, material) {
                    if (error) {
                        response.status(500).send(error);
                        return;
                    }
                    if (material) {
                        try {
                            if (categoriaId) {
                                material.categoria = categoriaA;
                            }
                            if (request.body.nombre) {
                                material.nombre = request.body.nombre;
                            }
                            if (request.body.descripcion) {
                                material.descripcion = request.body.descripcion;
                            }

                            material.foto = foto;

                            if (request.body.estatus === 1) {
                                material.estatus = "Activo";
                            }
                            material.save();
                            response.status(200).json({
                                message: "Se ha modificado correctamente"
                            })
                            return;
                        } catch (error) {
                            response.status(500).send(error);
                            return;
                        }


                    }
                    response.status(404).json({
                        message: 'No se encontro este registro.'
                    });
                });

            }
        });

    },
    delete: function(request, response) {
        var materialId = request.body.id;
        Material.findOne({ _id: materialId }, function(error, material) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            if (material) {
                try {
                    material.estatus = "Inactivo";
                    material.save();
                    response.status(200).json({
                        message: "Se ha inhabilitado correctamente"
                    })
                    return;
                } catch (error) {
                    response.status(500).send(error);
                    return;
                }

            }
            response.status(404).json({
                message: 'No se encontro este registro.'
            });
        });
    }
};