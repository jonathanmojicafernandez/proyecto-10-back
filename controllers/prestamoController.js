const Prestamos = require('../models/prestamos');
const Prestamo = require('../models/prestamos');
const Usuario = require('../models/usuarios');


module.exports = {
    getAll: function (request, response) {
        Prestamo.find({estatus: "Prestado" },function (error, prestamos) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            response.json(prestamos);
        })
    },
    add: function (request, response){
        let usuarioId = Number(request.body.idUsuario);
        Prestamo.find({estatus:{$in:["Reposicion","Adeudo","Prestado"]}, "usuario._id": usuarioId},  function (error, prestamos) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            if (prestamos.length === 0){
                Usuario.findOne({_id: usuarioId},function (error, usuario){
                    if (error) {
                        response.status(500).send(error);
                        return;
                    }
                    if (usuario) 
                    {
                        data = {
                            "inicio":request.body.inicio,
                            "fin":request.body.fin,
                            "estatus": "Prestado",
                            "usuario":usuario,
                            "entradas_prestamo":request.body.entradas_prestamo
                        }
                        let prestamo = new Prestamo(data);
                        prestamo.save();
                        return;
                    }
                 
                });
                response.status(200).json({
                    message: "Se ha registrado correctamente"
                })  
              
                return;
            }else{
                response.status(200).json({
                    message: "Cuenta con adeudos o reposiciones o prestamos"
                })
            }
        });
     
    },
    update: function (request, response){
        let idPrestamo = request.body.idPrestamo;
        Prestamo.findOne({_id: idPrestamo},function (error, prestamo){
            if (error) {
                response.status(500).send(error);
                return;
            }
            if (prestamo) 
            {   
                if(prestamo.fin !== request.body.fin){
                    prestamo.fin = request.body.fin;
                }
                prestamo.entradas_prestamo = request.body.entradas_prestamo;

                prestamo.save();
                response.status(200).json({
                    message: "Se ha modificado correctamente"
                })
                return;
            }
            response.status(404).json({
                message: 'No se encontro este registro.'
            });
        });

       
    },
    devolucion: function (request, response){
        let idPrestamo = request.body.idPrestamo;
        Prestamo.findOne({_id: idPrestamo},function (error, prestamo){
            if (error) {
                response.status(500).send(error);
                return;
            }
            if (prestamo) 
            {   
               prestamo.estatus = "Finalizado";
                prestamo.save();
                response.status(200).json({
                    message: "Se ha finalizado correctamente"
                })
                return;
            }
            response.status(404).json({
                message: 'No se encontro este registro.'
            });
        });
    },
    reposicion: function (request, response){
        let idPrestamo = request.body.idPrestamo;
        Prestamo.findOne({_id: idPrestamo},function (error, prestamo){
            if (error) {
                response.status(500).send(error);
                return;
            }
            if (prestamo) 
            {   
               prestamo.estatus = "Reposicion";
               prestamo.entradas_prestamo = request.body.entradas_prestamo;
                prestamo.save();
                response.status(200).json({
                    message: "Se ha registrado un adeudo correctamente"
                })
                return;
            }
            response.status(404).json({
                message: 'No se encontro este registro.'
            });
        });
    },
    getAllFinalizados: function (request, response) {
       
        Prestamo.find({estatus: "Finalizados" },function (error, prestamos) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            response.json(prestamos);
        })
    },
    getAlladeudo: function (request, response) {
        Prestamo.find({estatus: "Adeudo" },function (error, prestamos) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            response.json(prestamos);
        })
    },
    getAllReposicion: function (request, response) {
        
        Prestamo.find({estatus: "Reposicion" },function (error, prestamos) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            response.json(prestamos);
        })
    },

    chek:function (request, response){
        Prestamo.find({estatus:{$in:["Reposicion","Adeudo"]}, "usuario._id": 5},  function (error, prestamos) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            if (prestamos.length === 0){
                response.json({message : "Puede realizar prestamo", prestamos: prestamos});
            }else{
                response.json({message : "Presenta adeudos, reposicion", prestamos: prestamos});
            }
        })
    },
    changeDebt: function(){
        let fechaA = new Date();
        let dd = fechaA.getDate();
        let mm = fechaA.getMonth();
        let yyyy = fechaA.getFullYear();
     
        let f1 = new Date(yyyy, mm, dd);
        Prestamos.find({estatus: "Prestado" },function (error, pres) {
           if(pres){
                pres.forEach(element => {
                    Prestamo.findOne({_id: element._id}, function (error, pres) {
                        let d = new Date(pres.fin);
                        let dia =  d .getDate()+1;
                        let mes =  d .getMonth() ;
                        let anio = d .getFullYear();
                        let f2 = new Date(anio,mes,dia);
                        console.log("Entra");
                        console.log(f1);
                        console.log(f2);
                        let res = (f2 > f1);
                        console.log(res);
                        if(res == false){
                            pres.estatus = "Adeudo";
                            pres.save();
                            
                        }
                    });
               });
           }         
               
        })
      
    },
    
}