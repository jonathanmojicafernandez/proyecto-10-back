const Categoria = require('../models/categorias');

module.exports = {
    getAll: function(request,response){
        Categoria.find(function (error, categorias) {
            if (error) 
            {
                response.status(500).send(error);
                return;
            }
            response.json(categorias);
        })
    },
    get: function(request,response){
        var categoriaId = request.params.id;
        Categoria.findOne({_id: categoriaId},function (error, categoria)
        {
            if (error) {
                response.status(500).send(error);
                return;
            }
            if (categoria) 
            {
                response.json(categoria);
                return;
            }
            response.status(404).json({
                message: 'No se encontro este registro.'
            });
        });
    },
    
    add: function(request,response)
    {
        console.log(request.body);
        var newCategoria = new Categoria(request.body);
        newCategoria.save();
        response.status(201).send(newCategoria);
    },
    update: function(request,response){
        var categoriaId = request.params.id;
        Categoria.findOne({_id: categoriaId},function (error, categoria) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            if (categoria) {

                for (var property in request.body) {
                    if (request.body.hasOwnProperty(property)) {
                        if (typeof categoria[property] !== 'undefined') {
                            categoria[property] = request.body[property];
                        }
                    }
                }
                if (request.body.nombre) {
                    categoria.nombre = request.body.nombre;
                }
                categoria.save();
                response.json(categoria);
                return;
            }
            response.status(404).json({
                message: 'No se encontro este registro.'
            });
        });
    },
    delete: function(request,response){
        var categoriaId = request.params.id;
        Categoria.findOneAndRemove({_id: categoriaId},function (error) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            response.json(1);
        });
    }
};

