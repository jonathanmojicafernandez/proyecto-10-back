const Usuario = require('../models/usuarios');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Rango = require('../models/rangos');
const { cloudinary } = require("../utils/cloudinary");

module.exports = {
    getAllUsers: function (request, response) {//todos los usuarios
        Usuario.find(function (error, usuario) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            response.json(usuario);
        })
    },
    getAll: function (request, response) {//usuarios laboratoristas validados
        Usuario.find({ "rango.nombre": "Laboratorista", "estatus": "Aceptado" }, function (error, usuario) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            response.json(usuario);
        })
    },
    getUsuarios: function (request, response) {//usuarios filtrados por rol usuario y buscados por matricula
        var matricula = Number(request.body.matricula);
        Usuario.find({ "rango.nombre": "Usuario", matricula: matricula }, function (error, usuario) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            response.json(usuario);
        })
    },
    add: async function (request, response) {
        var foto = '';
        var rangoId = Number(request.body.rangoId);
        const { nombres, apellido1, apellido2, matricula, password, ciudad, colonia, cp, calle, numeroInt, numeroExt, estatus, fileSrt } = request.body;
        //console.log(request.body.foto);
        if (request.body.foto == '') {
            foto = './defecto.jpg';

        } else {
            try {
                const uploadedResponse = await cloudinary.uploader.upload(request.body.foto, {
                    upload_preset: 'ml_default'
                })
                //console.log(uploadedResponse);
                foto = uploadedResponse.url;
            } catch (error) {
                console.error(error);
                response.status(500).json({ err: 'no se pudo cargar la imagen' });
            }
        }

        if (!(matricula && password)) {
            return response.status(400).json({ response: "Es necesario llenar todos los campos" });
        }
        const userFind = await Usuario.findOne({ matricula: matricula });
        if (userFind) {
            return response.status(400).json({ response: "El usuario con esta matricula ya existe" });
        }
        const rango = await Rango.findOne({ id: rangoId });
        if (rango) {
            encryptedPassword = await bcrypt.hash(password, 10);
            var user = await Usuario.create({
                nombres,
                apellido1,
                apellido2,
                matricula: matricula,
                password: encryptedPassword,
                ciudad,
                colonia,
                cp,
                calle,
                numeroInt,
                numeroExt,
                estatus,
                foto,
                rango
            });
            const token = jwt.sign(
                { user_id: user._id, matricula },
                process.env.TOKEN_KEY,
                {
                    expiresIn: "2h",
                }
            );
            user.token = token;
            user.save();
            return response.status(200).json(user);
        }
    },
    update: async function (request, response) {
        let newRango = Rango;
        let usuarioId = request.body.idUsuario;
        let rangoId = request.body.rangoId;

        var foto = '';

        if (request.body.foto == './defecto.jpg') {
            foto = request.body.foto;
        } else {

            try {
                const uploadedResponse = await cloudinary.uploader.upload(request.body.foto, {
                    upload_preset: 'ml_default'
                })
                //console.log(uploadedResponse);
                foto = uploadedResponse.url;
            } catch (error) {
                console.error(error);
                response.status(500).json({ err: 'no se pudo cargar la imagen' });
            }
        }


        const rango = await Rango.findOne({ id: rangoId });
        if (rango) {
            newRango = new Rango(rango);
            const usuario = await Usuario.findOne({ _id: usuarioId });
            if (usuario) {
                try {
                    usuario.nombres = request.body.nombres;
                    usuario.apellido1 = request.body.apellido1;
                    usuario.apellido2 = request.body.apellido2;
                    usuario.matricula = request.body.matricula;
                    // if (request.body.password != null || request.body.password != undefined || request.body.password != '') {
                    //     encryptedPassword = await bcrypt.hash(request.body.password, 10);
                    //     usuario.password = encryptedPassword;
                    // }
                    // usuario.password = request.body.password;
                    usuario.ciudad = request.body.ciudad;
                    usuario.colonia = request.body.colonia;
                    usuario.cp = request.body.cp;
                    usuario.calle = request.body.calle;
                    usuario.numeroInt = request.body.numeroInt;
                    usuario.numeroExt = request.body.numeroExt
                    // usuario.estatus = request.body.estatus;
                    usuario.foto = foto;
                    usuario.rango = newRango;

                    usuario.save();
                    return response.status(200).json({
                        message: "Se ha modificado correctamente"
                    });
                } catch (error) {
                    console.log(error);
                }
            }
            return response.status(404).json({
                message: 'No se encontro este registro.'
            });

        }

    },
    accepted: function (request, response) {
        let usuarioId = Number(request.body.idUsuario);

        Usuario.findOne({ _id: usuarioId }, function (error, usuario) {
            if (error) {
                response.status(500).send(error);
            }
            if (usuario) {
                usuario.estatus = "Aceptado";
                usuario.save();
                response.status(200).json({
                    message: "Se ha aceptado el usuario correctamente"
                })
                return;
            }
        });
    },
    rejected: function (request, response) {
        let usuarioId = Number(request.body.idUsuario);

        Usuario.findOne({ _id: usuarioId }, function (error, usuario) {
            if (error) {
                response.status(500).send(error);
            }
            if (usuario) {
                usuario.estatus = "Rechazado";
                usuario.save();
                response.status(200).json({
                    message: "Se ha rechazado el usuario correctamente"
                })
                return;
            }
        });
    }
};