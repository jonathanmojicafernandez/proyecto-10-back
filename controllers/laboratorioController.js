const Laboratorio = require('../models/laboratorios');
const Usuario = require('../models/usuarios');
const Rango = require('../models/rangos');
// const usuarios = require('../models/usuarios');

module.exports = {
    getAll: function (request, response) {
        Laboratorio.find({estatus:"Activo"},function (error, laboratorios) {

            response.json(laboratorios);
        })
    },
    add: function (request, response) {
                
        // console.log('id '+request.body.idUsuario);        

        var usuarioId = Number(request.body.idUsuario);

        var laboratorio = Laboratorio;

        Usuario.findOne({_id: usuarioId},function (error, usuario){
            // console.log('usuario '+usuario);
            if (error) {
                response.status(500).send(error);
                return;
            }
            if (usuario) 
            {
                // console.log(usuario);
                // response.json(usuario);
                // laboratorio.nombre = request.body.nombre;
                // laboratorio.localizacion = request.body.localizacion;
                // laboratorio.usuario = usuario;

                data = {
                    "nombre":request.body.nombre,
                    "localizacion":request.body.localizacion,
                    "usuario":usuario
                }

                // data.usuario = usuario; //esta linea es por si no se asigna el objeto usuario en la data, la valriable "usuario" sería = {}
                // console.log(data);

                var lab = new Laboratorio(data);
                lab.save();
                response.json(data);

                return;
            }
            response.status(404).json({
                message: 'No se encontro este registro.'
            });
        });
        
        // var newLaboratorio = new Laboratorio(laboratorio);
        // console.log(newLaboratorio);

        // newLaboratorio.save();
        // response.status(201).send(newLaboratorio);
    },
    update: function (request, response){
        let usuarioA = Usuario;   
        let  usuarioId = request.body.idA;   
        let laboratorioId = request.body.id;   
       
      
        Usuario.findOne({_id: usuarioId},function (error, usuario){
            // console.log('usuario '+usuario);
            if (error) {
                response.status(500).send(error);
            }
            if (usuario) 
            {
                usuarioA = new Usuario(usuario);
                Laboratorio.findOne({_id: laboratorioId},function (error, laboratorio) {
                    if(error){
                        response.status(500).send(error);
                        return;
                    }
                    if(laboratorio){
                            if(usuarioId){
                              laboratorio.usuario = usuarioA;
                            }
                            if(request.body.nombre){
                                laboratorio.nombre = request.body.nombre;
                            }
                            if(request.body.localizacion){
                            laboratorio.localizacion = request.body.localizacion;
                            }

                            laboratorio.save();
                            response.status(200).json({
                                message: "Se ha modificado correctamente"
                            })
                            return;
                       
                       
                    }
                    response.status(404).json({
                        message: 'No se encontro este registro.'
                    });
                });
               
            }
        });
       
    },
    delete: function(request, response){
        let laboratorioId = request.body.id; 
        Laboratorio.findOne({_id: laboratorioId},function (error, laboratorio) {
            
            if(error){
                response.status(500).send(error);
                return;
            }
            if(laboratorio){
                try {
                    laboratorio.estatus = "Inactivo";
                    laboratorio.save();
                    response.status(200).json({
                        message: "Se ha inhabilitado correctamente"
                    })
                    return;
                } catch (error) {
                    response.status(500).send(error);
                    return;
                }
               
            }
            response.status(404).json({
                message: 'No se encontro este registro.'
            });
        });


    },
   
};