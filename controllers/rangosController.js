const Rango = require('../models/rangos');

module.exports ={
    getAll: function (request, response) {
        Rango.find(function (error, rangos) {

            response.json(rangos);
        })
    },
    
    add: function(request,response)
    {
        console.log(request.body);
        var newRango = new Rango(request.body);
        newRango.save();
        response.status(201).send(newRango);
    }
}