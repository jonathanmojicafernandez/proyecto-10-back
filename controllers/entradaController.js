
const Entrada = require('../models/entradas');
const Laboratorio = require('../models/laboratorios');
const Material = require('../models/materiales');

module.exports = {
    getAll: function (request, response) {
        Entrada.find({ estatus: "Disponible" }, function (error, entrada) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            response.json(entrada);
        })
    },
    add: function (request, response) {
        let laboratorioId = request.body.laboratorioId;
        let materialId = request.body.materialId;
        Laboratorio.findOne({ _id: laboratorioId }, function (error, laboratorio) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            if (laboratorio) {
                Material.findOne({ _id: materialId }, function (error, material) {
                    if (error) {
                        response.status(500).send(error);
                        return;
                    }
                    if (material) {
                        for (let i = 0; i < request.body.cantidad; i++) {
                            data = {
                                "costo": request.body.costo,
                                "clave": "",
                                "estatus": "Disponible",
                                "laboratorio": laboratorio,
                                "material": material
                            };
                            let entrada = new Entrada(data);
                            entrada.save(function (err, doc) {
                                Entrada.findOne({ _id: doc._id }, function (error, ent) {
                                    ent.clave = material.nombre.substring(0, 2) + "0" + ent._id;
                                    ent.save();
                                })
                            });
                        }
                    }

                })
                response.status(200).json({
                    message: "Se ha incertado correctamente"
                })
                return;
            }
            response.status(404).json({
                message: 'No se encontro este registro.'
            });
        });
    },
    update: function (request, response) {
        let idEn = request.body._id;
        Entrada.findOne({ _id: idEn }, function (error, entrada) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            if (entrada) {
                entrada.costo = request.body.costo;
                entrada.save();
                response.status(200).json({
                    message: "Se ha modificado correctamente"
                })
                return;
            }
            response.status(404).json({
                message: 'No se encontro este registro.'
            });
        });
    },
    delete: function (request, response) {
        let idEn = request.body._id;
        Entrada.findOne({ _id: idEn }, function (error, entrada) {
            if (error) {
                response.status(500).send(error);
                return;
            }
            if (entrada) {
                entrada.estatus = "Inactivo";
                entrada.save();
                response.status(200).json({
                    message: "Se ha eliminado correctamente"
                })
                return;
            }
            response.status(404).json({
                message: 'No se encontro este registro.'
            });
        });


    },
    search: function (request, response) {
        let id = request.body.id;
        Laboratorio.findOne({'usuario._id': id}, function(error, lab){
            if (error) {
                response.status(500).send(error);
                return;
            }
            if(lab){
                Entrada.findOne({clave: request.body.clave, estatus: "Disponible", 'laboratorio._id': lab._id}, function(error, entrada){
                    if (error) {
                        response.status(500).send(error);
                        return;
                    }
                    if (entrada) {
                        response.status(200).json({
                            entrada: entrada
                        })
                        return;
                    }
                    response.status(404).json({
                        message: 'No esta disponible o el material no esta en este laboratorio'
                    });
                    return;
                   
                });
            }
        });
    },
    getClaves: function(request, response){
        let cant = Number(request.params.cantidad);
        Entrada.find(function(error, entradas){
            if (error) {
                response.status(500).send(error);
                return;
            }
            if (entradas) {
                let claves = '';
                entradas.forEach(entrada => {
                    claves += entrada.clave+ " ";
                });
                response.json(claves);
                return;
            }
            response.status(404).json({
                message: 'No se encontro este registro.'
            });
           
        }).sort({$natural:-1}).limit(cant);
    },
    changeStatus: function(request, response){
        request.body.entradas_prestamo.forEach(entrada => {
            Entrada.findOne({_id: entrada._id}, function(error, entrad){
                if (error) {
                    response.status(500).send(error);
                    return;
                }
                if (entrad) {
                    entrad.estatus = request.body.estatus;
                    entrad.save();
                }
            });
        });
        response.status(200).json({
            message: 'Se ha cambiado el estatus'
        });
    }
}
