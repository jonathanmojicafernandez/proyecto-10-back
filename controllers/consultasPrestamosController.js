const Prestamo = require('../models/prestamos');
const usuarios = require('../models/usuarios');


module.exports = {
    getUser: function(request, response) {
        let token = request.headers['x-access-token'];
        usuarios.findOne({ token: token }, function(err, usuario) {
                if (err) throw err;
                if (usuario) {
                    response.json(usuario);
                }
            }

        );
    },
    getPrestamoActual: function(request, response) {
        let token = request.headers['x-access-token'];

        usuarios.findOne({ token: token }, function(err, usuario) {
            if (err) throw err;
            if (usuario) {
                Prestamo.findOne({ estatus: { $in: ["Reposicion", "Adeudo", "Prestado"] }, 'usuario._id': usuario._id }, function(error, prestamo) {
                    if (err) throw err;
                    if (prestamo) {
                        response.json(prestamo);
                        return;
                    }
                    response.status(404).json({
                        message: 'No se encontro este registro.'
                    });
                });
            }
        });

    },
    getHistorialPrestamo: function(request, response) {
        let token = request.headers['x-access-token'];

        usuarios.findOne({ token: token }, function(err, usuario) {
            if (err) throw err;
            if (usuario) {
                Prestamo.find({ 'usuario._id': usuario._id }, function(error, prestamo) {
                    if (err) throw err;
                    if (prestamo) {
                        response.json(prestamo);
                        return;

                    }
                    response.status(404).json({
                        message: 'No se encontro este registro.'
                    });
                });
            }
        });
    }
}