const Usuario = require('../models/usuarios');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {
    login: async function(request,response){
        try{
            const {matricula,password} = request.body;
            if (!(matricula && password)) {
              const objeto = {
                "code": 400,
                "response": "Debes llenar todos los datos"
              }
              return response.status(200).send(objeto);
            }
            const user = await Usuario.findOne({matricula});
           
              if (user && (await bcrypt.compare(password, user.password))) {
                // Create token
                if(user.rango.id != 1)
                {
                  const token = jwt.sign(
                    { user_id: user._id, matricula},
                    process.env.TOKEN_KEY,
                    {
                      expiresIn: "2h",
                    }
                  );
            
                  // save user token
                  user.token = token;
                  user.save();
                  const objeto = {
                    "code": 200,
                    "response": user
                  }
                  return response.status(200).json(objeto);
                }else{
                  const objeto = {
                    "code": 400,
                    "response": "No puedes acceder a esta seccion"
                  }
                  return response.status(200).send(objeto);
                }
              }else{
                const objeto = {
                  "code": 400,
                  "response": "Usuario o contraseña incorrectos"
                }
                return response.status(200).json(objeto);
              }
            
        }catch(e){
            console.log(e);
        }
    }
};