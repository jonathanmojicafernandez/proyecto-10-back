const Entrada = require('../models/entradas');
const Material = require('../models/materiales');



module.exports = {
    getLabMat: function(request, respons) {
        Entrada.aggregate([{
            "$group": {
                _id: '$laboratorio',
                materiales: {
                    $push: '$$ROOT'
                }
            }
        }], function(err, results) {
            if (err) throw err;
            respons.json(results);
        })
    },
    getCatMat: function(request, response) {
        Entrada.aggregate([{
            "$group": {
                _id: '$material.categoria',
                materiales: { $push: '$$ROOT' }
            }
        }], function(err, results) {
            if (err) throw err;
            response.json(results);
        })
    },
    getProdcuLabCate: function(request, response) {
        Entrada.aggregate([{
            "$group": {
                _id: '$material',
                materiales: { $push: '$$ROOT' }
            }
        }], function(err, results) {
            if (err) throw err;
            response.json(results);
        })
    },
    getBuscarNombreMaterial: function(request, response) {
        console.log(request.body.nombre);
        // Material.createIndex({ "$**": "text" });
        Material.find({
            $search: request.body.nombre,
            $caseSensitive: false
        }, function(err, results) {
            if (err) throw err;
            response.json(results);
        })
    }
}