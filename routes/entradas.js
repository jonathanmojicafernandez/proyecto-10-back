var express = require('express');
const entradaController = require('../controllers/entradaController');

var entradaRouter = express.Router();

entradaRouter.route('/entrada').get(entradaController.getAll);
entradaRouter.route('/entrada/add').post(entradaController.add);
entradaRouter.route('/entrada/update').post(entradaController.update);
entradaRouter.route('/entrada/delete').post(entradaController.delete);
entradaRouter.route('/entrada/search').post(entradaController.search);
entradaRouter.route('/entrada/getKeys/:cantidad').get(entradaController.getClaves);
entradaRouter.route('/entrada/changeStatus').post(entradaController.changeStatus);

module.exports = entradaRouter;