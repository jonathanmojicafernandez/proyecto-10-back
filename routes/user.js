const userController = require('../controllers/userController');
var express = require('express');

var userRouter = express.Router();

userRouter.route('/user/add').post(userController.add);
userRouter.route('/user/update').post(userController.update);
userRouter.route('/user/login').post(userController.login);



module.exports = userRouter;