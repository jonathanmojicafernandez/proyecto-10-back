var express = require('express');
var Item = require('../models/materiales');
const materialController = require('../controllers/materialController');

var materialRouter = express.Router();

materialRouter.route('/materiales').get(materialController.getAll);
materialRouter.route('/materiales/add').post(materialController.add);
materialRouter.route('/materiales/update').post(materialController.update);
materialRouter.route('/materiales/delete').post(materialController.delete);


module.exports = materialRouter;