var express = require('express');
var Item = require('../models/prestamos');
const prestamosController = require('../controllers/prestamoController');

var prestamoRouter = express.Router();

prestamoRouter.route('/prestamos').get(prestamosController.getAll);
prestamoRouter.route('/prestamos/add').post(prestamosController.add);
prestamoRouter.route('/prestamos/update').post(prestamosController.update);
prestamoRouter.route('/prestamos/devolucion').post(prestamosController.devolucion);
prestamoRouter.route('/prestamos/check').get(prestamosController.chek);
prestamoRouter.route('/prestamos/changeReposicion').post(prestamosController.reposicion);

prestamoRouter.route('/prestamos/adeudos').get(prestamosController.getAlladeudo);
prestamoRouter.route('/prestamos/reposicion').get(prestamosController.getAllReposicion);
module.exports = prestamoRouter;