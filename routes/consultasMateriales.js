var express = require('express');
var Item = require('../models/materiales');
const consultasController = require('../controllers/consultasMaterialesController');

var consultasMaterialRouter = express.Router();

consultasMaterialRouter.route('/consultar/laboratoriosproductos').get(consultasController.getLabMat);
consultasMaterialRouter.route('/consultar/categoriaproductos').get(consultasController.getCatMat);
consultasMaterialRouter.route('/consultar/buscarMaterial').get(consultasController.getBuscarNombreMaterial);
consultasMaterialRouter.route('/consultar/productosLabCate').get(consultasController.getProdcuLabCate);


module.exports = consultasMaterialRouter;