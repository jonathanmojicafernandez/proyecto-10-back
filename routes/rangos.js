var express = require('express');
var Item = require('../models/rangos');
const rangoController = require('../controllers/rangosController');

var rangoRouter = express.Router();

rangoRouter.route('/rangos').get(rangoController.getAll);
rangoRouter.route('/rangos/add').post(rangoController.add);

module.exports = rangoRouter;


