var express = require('express');
var Item = require('../models/materiales');
const consultasController = require('../controllers/consultasPrestamosController');

var consultasPrestamoRouter = express.Router();

consultasPrestamoRouter.route('/consultar/prestamoActual').get(consultasController.getPrestamoActual);
consultasPrestamoRouter.route('/consultar/prestamoHistorial').get(consultasController.getHistorialPrestamo);
consultasPrestamoRouter.route('/consultar/usuario').get(consultasController.getUser);


module.exports = consultasPrestamoRouter;