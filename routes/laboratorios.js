var express = require('express');
var Item = require('../models/laboratorios');
const laboratorioController = require('../controllers/laboratorioController');

var laboratorioRouter = express.Router();

laboratorioRouter.route('/laboratorios').get(laboratorioController.getAll);
laboratorioRouter.route('/laboratorios/add').post(laboratorioController.add);
laboratorioRouter.route('/laboratorios/update').post(laboratorioController.update);
laboratorioRouter.route('/laboratorios/delete').post(laboratorioController.delete);

module.exports = laboratorioRouter;