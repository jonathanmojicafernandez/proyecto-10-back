var express = require('express');
var Item = require('../models/categorias');
const categoriaController = require('../controllers/categoriaController');

var categoriaRouter = express.Router();

categoriaRouter.route('/categorias').get(categoriaController.getAll);
categoriaRouter.route('/categorias/add').post(categoriaController.add);
categoriaRouter.route('/categorias/update/:id').post(categoriaController.update);
categoriaRouter.route('/categorias/delete/:id').post(categoriaController.delete);
categoriaRouter.route('/categorias/get/:id').get(categoriaController.get);

module.exports = categoriaRouter;
