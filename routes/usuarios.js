const usuariosController = require('../controllers/usuariosController');
var express = require('express');

var usuariosRouter = express.Router();

usuariosRouter.route('/usuarios').get(usuariosController.getAll);//admin, lab
usuariosRouter.route('/usuariosNoFilter').get(usuariosController.getAllUsers);//todos
usuariosRouter.route('/usuariosU').post(usuariosController.getUsuarios);//solo usuarios buscados por matricula
usuariosRouter.route('/usuarios/add').post(usuariosController.add);
usuariosRouter.route('/usuarios/update').post(usuariosController.update);
usuariosRouter.route('/usuarios/accepted').post(usuariosController.accepted);
usuariosRouter.route('/usuarios/rejected').post(usuariosController.rejected);

module.exports = usuariosRouter;